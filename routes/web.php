<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
|--------------------------------------------------------------------------
*/

//Главная
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


//Роуты профиль юзера для просмотра, написание и т.д тикетов
Route::group(['middleware' => ['role:user'], 'prefix' => 'profile'], function(){

    //Get Ticket User
    Route::get('/tickets', 'ProfileController@index');

    //Create
    Route::get('/tickets/create', 'ProfileController@support');

    //Insert Ticket
    Route::post('/tickets', 'MainController@createTicket');

    //Single tiket
    Route::get('/tickets/{id}', 'ProfileController@singleTicket');

    //Create Comment
    Route::post('/tickets/{id}', 'MainController@createComment');

});

//Group Admin Routes
Route::group(['middleware' => ['role:admin'], 'prefix' => 'admin'], function(){

    //Get All Users
    Route::get('/users', 'Admin\UsersController@index');

    //Edit User Data
    Route::put('/users/{id}/edit', 'Admin\UsersController@edit');

    //Create User
    Route::get('/users/create', 'Admin\UsersController@create');

    //Insert User
    Route::post('/users/create', 'Admin\UsersController@insert');
    
    //Destroy User
    Route::delete('/users/{id}/delete', 'Admin\UsersController@destroy');

    //Get Single User
    Route::get('/users/{id}', 'Admin\UsersController@show');

    /*-----------------------------------------------------------------------*/

    //Get All Tickets
    Route::get('/tickets', 'Admin\TicketsController@index');

    //Create Ticket
    Route::get('/tickets/create/{userId}', 'Admin\TicketsController@create');

    //Insert Ticket
    Route::post('/tickets', 'MainController@createTicket');

    //Edit Status Tickets
    Route::put('/tickets/{id}/edit', 'Admin\TicketsController@edit');

    //Delete Tickets
    Route::delete('/tickets/{id}/delete', 'Admin\TicketsController@destroy');

    //Create Comment
    Route::post('/tickets/{id}', 'MainController@createComment');

    //Get Single Ticket
    Route::get('/tickets/{id}', 'Admin\TicketsController@show');

});