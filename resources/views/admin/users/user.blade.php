@extends('layouts.app')

@section('content')

<div class = "container">
    @include('admin.menu')
    <div style = "float: left;display: inline-block; width: 75%;">
        <div class="col-md-13">
            <div class="panel panel-default profile">
                <div class="panel-heading">Дата последнего посещения: {{ $user->last_activity_at }}</div>
                <div class="panel-body">
                    @if(session('check'))
                    <div class = "alert alert-info">{{ session('check') }}</div>
                    @endif
                    <form class="form-horizontal" method="POST" action="/admin/users/{{ $user->id }}/edit">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Имя</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="first_name" value="{{ $user->first_name }}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Фамилия</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="last_name" value="{{ $user->last_name }}" required>

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value = "{{ $user->email }}" required disabled>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                            <label for="company" class="col-md-4 control-label">Компания</label>

                            <div class="col-md-6">
                                <input id="company" type="text" class="form-control" value = "{{ $user->company }}" name="company" required>

                                @if ($errors->has('company'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('number_phone') ? ' has-error' : '' }}">
                            <label for="number_phone" class="col-md-4 control-label">Номер телефона</label>

                            <div class="col-md-6">
                                <input id="number_phone" type="text" class="form-control" value = "{{ $user->number_phone }}" name="number_phone" required>

                                @if ($errors->has('number_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('site') ? ' has-error' : '' }}">
                            <label for="site" class="col-md-4 control-label">Сайт</label>

                            <div class="col-md-6">
                                <input id="site" type="text" class="form-control" value = "{{ $user->site }}" name="site" required>

                                @if ($errors->has('site'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('site') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" name = "edit" value = "edit" class="btn btn-primary">
                                    Изменить
                                </button>
                            </div>
                        </div>
                    </form>
                    <form method = "post" action = "/admin/users/{{ $user->id }}/delete">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <p><button class = "btn btn-danger" type = "submit" value = "delete" name = "delete">Удалить пользователя</button></p>
                    </form>
                        <p><a class = "btn btn-primary" href = "/admin/tickets/create/{{ $user->id }}">Написать тикет</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
