@extends('layouts.app')

@section('content')

<div class = "container">
    @include('admin.menu')
    <div style = "float: left;display: inline-block; width: 75%;">
        <div class="col-md-13">
            <div class="panel panel-default profile">
                <div class="panel-heading">Список клиентов</div>
                <div class="panel-body">
                    @if(session('first_name') and session('last_name'))
                        <div class = "alert alert-info">Пользователь: {{ session('first_name') }} {{ session('last_name') }} успешно удален</div>
                    @endif
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        @foreach($users as $u)
                            <hr>
                            <p><a href = "/admin/users/{{ $u->id }}">{{ $u->first_name }} {{ $u->last_name }}</a></p>
                            <hr>
                        @endforeach
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
