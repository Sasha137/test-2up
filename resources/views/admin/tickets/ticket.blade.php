@extends('layouts.app')

@section('content')

<div class = "container">
    @include('admin.menu')
    <div style = "float: left;display: inline-block; width: 75%;">
        <div class="col-md-13">
            <div class="panel panel-default profile">
                <div class="panel-heading">{{ $ticket->title }}</div>
                <div class="panel-body">
                    @if(session('edit'))
                        <div class = "alert alert-info">{{ session('edit') }}</div>
                    @endif
                    <form method = "post" action = "/admin/tickets/{{ $ticket->id }}/delete">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button class = "btn btn-danger" type = "submit" value = "delete" name = "delete">Удалить тикет</button>
                    </form>

                    <p><b>Имя Клиента:</b> {{ $ticket->user->first_name }}</p>
                    <p><b>Фамилия Клиента:</b> {{ $ticket->user->last_name }}</p>
                
                    
                    <form method = "post" action = "/admin/tickets/{{ $ticket->id }}/edit">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <p>
                            <b>Статус:</b>
                            <select name="status">
                                <option {{ $ticket->status->status == 0 ? 'selected=selected' : '' }} value="0">Новый</option>
                                <option {{ $ticket->status->status == 1 ? 'selected=selected' : '' }} value="1">Просмотрен</option>
                                <option {{ $ticket->status->status == 2 ? 'selected=selected' : '' }} value="2">Оплачен</option>
                                <option {{ $ticket->status->status == 3 ? 'selected=selected' : '' }} value="3">Выполнен</option>
                                <option {{ $ticket->status->status == 4 ? 'selected=selected' : '' }} value="4">Закрыт</option>
                            </select>

                        </p>
                        @if ($errors->has('status'))
                            <span class="help-block">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                        <p><b>Название:</b></p>
                        <p><textarea name="title" id="" cols="50" rows="5">{{ $ticket->title }}</textarea></p>
                        @if ($errors->has('title'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('title') }}</strong>
                            </div>
                        @endif
                        <p><b>Описание:</b></p>
                        <center><textarea name="description" id="" cols="114" rows="5">{{ $ticket->description }}</textarea></center>
                        @if ($errors->has('description'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('description') }}</strong>
                            </div>
                        @endif
                        <p>
                            <button class = "btn btn-primary" type = "submit" name = "edit" value = "edit">Изменить</button>
                        </p>

                    </form>


                    @if ($ticket->comments->isNotEmpty())
                        <h3><center>Комментарии</center></h3>
                        <br>
                        @foreach($ticket->comments as $c)
                            @if($c->author_id == auth()->user()->id)
                                <hr>
                                <p>Вы:</p>
                                <p>{{ $c->comment }}</p>
                                
                            @else
                                <hr>
                                <p>{{ $ticket->user->first_name }} {{ $ticket->user->last_name }}</p>
                                <p>{{ $c->comment }}</p>

                            @endif
                        @endforeach
                    @endif
                        

                    <h3><center>Написать комментарий</center></h3>
                    @if(session('comment'))
                        <div class = "alert alert-info">{{ session('comment') }}</div>
                    @endif
                    @if ($errors->has('comment'))
                        <div class="alert alert-danger">
                            <strong>{{ $errors->first('comment') }}</strong>
                        </div>
                    @endif
                    <form method = "post" action = "/admin/tickets/{{ $ticket->id }}">
                        {{ csrf_field() }}
                        <input type = "hidden" name = "ticket_id" value = "{{ $ticket->id }}">
                        <center><textarea maxlength = "140" name = "comment" cols="100" rows="10">{{ old('comment') }}</textarea></center>
                        <center><button class = "btn btn-primary" type = "submit" value = "Отправить" name = "send">Отправить</button></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
