@extends('layouts.app')

@section('content')

<div class = "container">
    @include('admin.menu')
    <div style = "float: left;display: inline-block; width: 75%;">
        <div class="col-md-13">
            <div class="panel panel-default profile">
                <div class="panel-heading">Написать тикет</div>
                <div class="panel-body">
                @if(session('check'))
                    <div class = "alert alert-info">{{ session('check') }}</div>
                @endif
                <form class="form-horizontal" method="POST" action="/admin/tickets">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="title" class="col-md-4 control-label">Название тикета</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <div class="alert alert-danger"><strong>{{ $errors->first('title') }}</strong></div>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-md-4 control-label">Сообщение</label>

                            <div class="col-md-6">
                                <textarea name = "description" cols="53" rows="10" required>{{ old('description') }}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <div class="alert alert-danger"><strong>{{ $errors->first('description') }}</strong></div>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <input type = "hidden" name = "ticket_to" value = "{{ $userId }}">

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button value = "Написать" name = "write" type="submit" class="btn btn-primary">
                                    Написать
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
