@extends('layouts.app')

@section('content')

<div class = "container">
    @include('admin.menu')
    <div style = "float: left;display: inline-block; width: 75%;">
        <div class="col-md-13">
            <div class="panel panel-default profile">
                <div class="panel-heading">Список Тикетов</div>
                <div class="panel-body">
                    <form method = "get" action = "">
                        <p><button class = "btn btn-primary" type = "submit" value = "open" name = "status">Открытые</button>
                        <button class = "btn btn-primary" type = "submit" value = "close" name = "status">Закрытые</button></p>
                    </form>

                    <br>

                    @foreach($tickets as $t)
                        <hr>
                        <a href = "/admin/tickets/{{ $t->id }}">
                            <p>Название: {{ $t->title }}</p>
                            <p>Описание: {{ $t->description }}</p>
                            <p>Статус: {{ $status[$t->status->status] }}</p>
                        </a>
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
