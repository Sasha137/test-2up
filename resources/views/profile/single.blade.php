@extends('layouts.app')

@section('content')

<div class = "container">
    @include('profile.menu')
    <div style = "float: left;display: inline-block; width: 75%;">
        <div class="col-md-13">
            <div class="panel panel-default profile">
                <div class="panel-heading">{{ $ticket->title }}</div>
                <div class="panel-body">

                    @if($ticket->status->status)
                        <p><b>Имя Администратора:</b> {{ $ticket->admin->first_name }}</p>
                        <p><b>Фамилия Администратора:</b> {{ $ticket->admin->last_name }}</p>
                    @endif


                    <p><b>Статус:</b> {{ $status[$ticket->status->status] }}</p>
                    <p><b>Описание:</b>
                        {{ $ticket->description }}
                    </p>

                    @if($ticket->comments->isNotEmpty())
                        <h3><center>Комментарии</center></h3>
                        <br>
                        @foreach($ticket->comments as $c)
                            @if($c->author_id == auth()->user()->id)
                                <hr>
                                <p>Вы:</p>
                                <p>{{ $c->comment }}</p>
                                
                            @else
                                <hr>
                                <p>{{ $ticket->admin->first_name }} {{ $ticket->admin->last_name }}</p>
                                <p>{{ $c->comment }}</p>

                            @endif
                        @endforeach
                    @endif
                    

                    <h3><center>Написать комментарий</center></h3>
                    @if(session('check'))
                        <div class = "alert alert-info">{{ session('check') }}</div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method = "post" action = "/profile/tickets/{{ $ticket->id }}">
                        {{ csrf_field() }}
                        <input type = "hidden" name = "ticket_id" value = "{{ $id }}">
                        <center><textarea name = "comment" cols="100" rows="10">{{ old('comment') }}</textarea></center>
                        <center><button class = "btn btn-primary" type = "submit" value = "Отправить" name = "send">Отправить</button></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
