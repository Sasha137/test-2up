@extends('layouts.app')

@section('content')

<div class = "container">
    @include('profile.menu')
    <div style = "float: left;display: inline-block; width: 75%;">
        <div class="col-md-13">
            <div class="panel panel-default profile">
                <div class="panel-heading">Список тикетов</div>
                <div class="panel-body">

                    @foreach($tickets as $t)
                        @if($t->owner->user->id == auth()->user()->id)
                            <p>Автор: Вы</p>
                        @else
                            <p>Автор: {{ $t->owner->user->first_name }} {{ $t->owner->user->last_name }}</p>
                        @endif
                        <p><a href = "/profile/tickets/{{ $t->id }}">{{ $t->title }}</a></p>
                        <p>Дата создания: {{ $t->created_at }}</p>
                        <p>Статус: {{ $status[$t->status->status] }}</p>
                        <hr>
                    @endforeach

                    <center>{{ $tickets->links() }}</center>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
