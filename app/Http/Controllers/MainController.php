<?php

namespace Ticket2Up\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Ticket2Up\Ticket;
use Ticket2Up\TicketComment;
use Ticket2Up\TicketStatus;
use Ticket2Up\TicketOwner;
use Ticket2Up\AdminTotalTicket;

class MainController extends Controller
{
    /*
    *   Для добавления комментариев
    */
    public function createComment(Request $request){
        if($request->has('send')){
            $validator = Validator::make($request->only('comment'), [
                'comment' => 'required|string|max:140'
            ]);

            if($validator->fails()){
                return back()->withErrors($validator)->withInput();
            }else{

                /*
                *   Если админ проверяем кол-во его коментариев. 
                *   Если это первый коментарий меняем статус тикета с Нового на Просмотрен
                */
                if(auth()->user()->hasRole('admin')){
                    $count = TicketComment::where('ticket_id', $request->ticket_id)
                        ->where('author_id', auth()->user()->id)
                    ->count();

                    if(!$count){
                        TicketStatus::where('ticket_id', $request->ticket_id)
                            ->update([
                                'status' => 1
                            ]);
                    }
                }

                TicketComment::create([
                    'comment' => $request->comment,
                    'author_id' => auth()->user()->id,
                    'ticket_id' => $request->ticket_id
                ]);

                return back()->with('comment', 'Комментарий успешно добавлен');
            }
        }
    }

    /*
    *   Написать тикет
    */
    public function createTicket(Request $request){
        if($request->has('write')){
            $validator = Validator::make($request->only('title', 'description'), [
                'title' => 'required|max:255|string|min:6',
                'description' => 'required|max:10000|string|min:20'
            ]);

            if($validator->fails()){
                return back()->withErrors($validator)->withInput();
            }else{
                //Добавляем тикет
                $add_ticket = Ticket::create([
                    'title' => $request->title,
                    'description' => $request->description,
                    'user_id' => auth()->user()->hasRole('user') ? auth()->user()->id : $request->ticket_to,
                    'admin_id' => auth()->user()->hasRole('admin') ? auth()->user()->id : $request->ticket_to
                ])->id;
                
                //Ставим админу инкремент на кол - во тикетов
                if(auth()->user()->hasRole('admin')){
                    AdminTotalTicket::where('admin_id', auth()->user()->id)->increment('amt_ticket');
                }else{
                    AdminTotalTicket::where('admin_id', $request->ticket_to)->increment('amt_ticket');
                }

                //Ставим статус тикета
                TicketStatus::create([
                    'ticket_id' => $add_ticket
                ]);

                //Ставим владельца тикета
                TicketOwner::create([
                    'ticket_id' => $add_ticket,
                    'owner_id' => auth()->user()->id
                ]);

                return back()->with('check', 'Ваш тикет отправлен, ожидайте ответа');
            }
        }
    }
}
