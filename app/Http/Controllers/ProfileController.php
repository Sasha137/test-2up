<?php

namespace Ticket2Up\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Ticket2Up\Ticket;
use Ticket2Up\Role;
use Ticket2Up\CommentTicket;

class ProfileController extends Controller
{

    /*
    *   Вывод всех тикетов
    */
    public function index()
    {
        $variable = [];

        //Достали тикеты пользователя
        $variable['tickets'] = Ticket::select('created_at', 'title', 'id', 'admin_id')
            ->with(['status', 'owner' => function($query){
                $query->with(['user' => function($query){
                    $query->addSelect('first_name', 'last_name', 'id');
                }]);
            }])
            ->where('user_id', auth()->user()->id)
            ->orderBy('updated_at', 'desc')
            ->paginate(5);
            
        //Для статуса тикета
        $variable['status'] = [
            '0' => 'Новый',
            '1' => 'Просмотрен',
            '2' => 'Оплачен',
            '3' => 'Выполнен',
            '4' => 'Закрыт'
        ];
        
        return view('profile.home', $variable);
    }

    /*
    *   Написать тикет
    */
    public function support(){   
        /*
        *   Достаем для определения админа который будет отвечать
        */
        $admin = Role::with(['byRole' => function($query){
            $query->with(['amtTicket' => function($query){
                $query->addSelect('amt_ticket', 'admin_id');
            }])->addSelect('id');
        }])->where('name', 'admin')
        ->select('id')
        ->first(); 

        $total_amt = [];

        foreach($admin->byRole as $a){
            $total_amt[$a->amtTicket->admin_id] = $a->amtTicket->amt_ticket;
        }

        $min = min($total_amt);

        $variable['admin_id'] = array_search($min, $total_amt);

        return view('profile.support', $variable);

    }

    //Вывод одного тикета
    public function singleTicket($id){
        $variable = [];
        
        //Single Tickets, Admin Ticket, Comments Ticket
        $variable['ticket'] = Ticket::where('id', $id)
            ->where('user_id', auth()->user()->id)
            ->select('title', 'description', 'id', 'admin_id')
            ->with(
                [
                    'admin' => function($query){
                        $query->select('id', 'last_name', 'first_name');
                    }, 
                    'comments' => function($query){
                        $query->select('comment', 'author_id', 'ticket_id')
                        ->orderBy('created_at', 'desc');
                    },
                    'status'
                ]
            )
        ->firstOrFail();

        //Для статуса тикета
        $variable['status'] = [
            '0' => 'Новый',
            '1' => 'Просмотрен',
            '2' => 'Оплачен',
            '3' => 'Выполнен',
            '4' => 'Закрыт'
        ];

        $variable['id'] = $id;

        return view('profile.single', $variable);
    }
}
