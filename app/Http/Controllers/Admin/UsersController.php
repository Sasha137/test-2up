<?php

namespace Ticket2Up\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Ticket2Up\Http\Controllers\Controller;
use Validator;
use Ticket2Up\Role;
use Ticket2Up\AdminTotalTicket;
use Ticket2Up\Ticket;
use Ticket2Up\User;

class UsersController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $data = [];
        
        $data['users'] = Role::find(1)->byRole()->paginate(10);

        return view('admin.users.index', $data);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $data = [];
        
        $data['user'] = Role::find(1)->byRole()->where('id', $id)->firstOrFail(); 

        return view('admin.users.user', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
         return view('admin.users.create');
     }

     public function insert(Request $request)
     {
        if($request->has('create')){
            $validator = Validator::make($request->all(), [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'company' => 'required|string|max:255',
                'number_phone' => 'required|string|max:255',
                'site' => 'required|string|max:255|active_url',
                'password' => 'required|string|min:8|confirmed'
            ]);

            if($validator->fails()){
                return redirect($request->path())->withErrors($validator)->withInput();
            }else{
                User::create([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'company' => $request->company,
                    'number_phone' => $request->number_phone,
                    'site' => $request->site,
                    'password' => bcrypt($request->password)
                ])->roles()->attach(1);

                return redirect($request->path())->with('check', 'Пользователь успешно создан');
            }
         }
     }
 
    /**
    * Show the form for editing the specified resource.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit(Request $request, $id)
    {
        if($request->has('edit')){
            $validator = Validator::make($request->all(), [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'company' => 'required|string|max:255',
                'number_phone' => 'required|string|max:255',
                'site' => 'required|string|max:255|active_url'
            ]);

            if($validator->fails()){
                return back()->withErrors($validator);
            }else{
                User::find($id)->update([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'company' => $request->company,
                    'number_phone' => $request->number_phone,
                    'site' => $request->site
                ]);

                return back()->with('check', 'Данные успешно изменены');
            }
        }
    }
 
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id)
    {
        if($request->has('delete')){
            //Считаем сколько у юзера открытых тикетов
            $count = Ticket::where('user_id', $id)->whereHas('status', function ($query) {
                $query->where('status', '<>', 4);
            })->count();

            //Уменьшаем кол - во тикетов у админа
            $adminTotal = AdminTotalTicket::where('admin_id', auth()->user()->id)->first();

                $adminTotal->amt_ticket = $adminTotal->amt_ticket - $count;

            $adminTotal->save();

            //Удаляем пользователя и все, что с ним связано Комментарии, Тикеты и т.д и т.п
            User::find($id)->delete();

            return redirect('/admin/users');
        }
    }
}
