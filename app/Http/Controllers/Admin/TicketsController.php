<?php

namespace Ticket2Up\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Ticket2Up\Http\Controllers\Controller;
use Ticket2Up\Ticket;
use Ticket2Up\TicketStatus;
use Ticket2Up\AdminTotalTicket;
use Ticket2Up\Role;
use Validator;
use Carbon\Carbon;

class TicketsController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $data = [];

        if($request->has('status') and $request->status == 'open' or $request->status == 'close'){

            if($request->status == 'open'){
                $status = [0, 1, 2, 3];
            }elseif($request->status == 'close'){
                $status = [4];
            }

            $data['tickets'] = Ticket::where('admin_id', auth()->user()->id)
            ->whereHas('status', function ($query) use ($status) {
                $query->whereIn('status', $status);
            })
            ->orderBy('created_at', 'desc')
            ->get();

        }else{

            $data['tickets'] = Ticket::with('status')
            ->where('admin_id', auth()->user()->id)
            ->orderBy('created_at', 'desc')
            ->get();

        }

        //Для статуса тикета
        $data['status'] = [
            '0' => 'Новый',
            '1' => 'Просмотрен',
            '2' => 'Оплачен',
            '3' => 'Выполнен',
            '4' => 'Закрыт'
        ];
        
        return view('admin.tickets.tickets', $data);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $data = [];

        //Single Tickets, User Ticket, Comments Ticket
        $data['ticket'] = Ticket::where('id', $id)
            ->where('admin_id', auth()->user()->id)
            ->select('title', 'description', 'id', 'user_id')
            ->with(
                [
                    'user' => function($query){
                        $query->select('id', 'last_name', 'first_name');
                    }, 
                    'comments' => function($query){
                        $query->select('comment', 'author_id', 'ticket_id')
                        ->orderBy('created_at', 'desc');
                    },
                    'status'
                ]
                )
        ->firstOrFail();

        return view('admin.tickets.ticket', $data);
    }
 
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create($userId)
    {
        $data = ['userId' => $userId];
    
        return view('admin.tickets.create', $data);
    }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit(Request $request, $id)
     {
        if($request->has('edit')){
            $validator = Validator::make($request->except('_token', '_method'), [
                'title' => 'required|max:255|string|min:6',
                'description' => 'required|max:10000|string|min:20',
                'status' => 'required|max:4|min:0|integer'
            ]);

            if($validator->fails()){
                return back()->withErrors($validator);
            }else{
                
                $status = TicketStatus::select('status')
                    ->where('ticket_id', $id)
                    ->where('status', '<>', $request->status)
                ->first();

                if($status){
                    //Если тикет уже закрыт открываем его
                    if($status->status == 4){
                        $minusTicket = AdminTotalTicket::where('admin_id', auth()->user()->id)
                        ->increment('amt_ticket');

                        //Делаем тикет как ново созданный
                        $updateTimestamp = Ticket::find($id);
                        $updateTimestamp->created_at = Carbon::now();
                        $updateTimestamp->save();
                    }

                    //Если тикет открыт закрываем его
                    if($request->status == 4){
                        $minusTicket = AdminTotalTicket::where('admin_id', auth()->user()->id)
                        ->decrement('amt_ticket');
                    }

                    TicketStatus::where('ticket_id', $id)->update($request->only('status'));
                }

                Ticket::find($id)->update($request->only('title', 'description'));

                return back()->with('edit', 'Данные успешно измененны');
            }
        }
     }
 
    /**
    * Remove the specified resource from storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id)
    {
        if($request->has('delete')){
            AdminTotalTicket::where('admin_id', auth()->user()->id)->decrement('amt_ticket');
            
            Ticket::find($id)->delete();

            return redirect('admin/tickets');
        }
    }
}
