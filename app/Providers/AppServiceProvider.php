<?php

namespace Ticket2Up\Providers;

use Illuminate\Support\ServiceProvider;
use Ticket2Up\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Ticket2Up\Role;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Для сохранения последнего визита пользователя
        view()->composer('*', function(){
            if(auth()->check()){
                $user = User::find(auth()->user()->id)
                ->update(['last_activity_at' => Carbon::now()]);   
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
