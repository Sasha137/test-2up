<?php

namespace Ticket2Up;

use Illuminate\Database\Eloquent\Model;

class TicketOwner extends Model
{
    public $timestamps = false;

    protected $fillable = ['ticket_id', 'owner_id'];

    public function user()
    {
        return $this->hasOne('Ticket2Up\User', 'id', 'owner_id');
    }
}