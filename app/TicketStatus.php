<?php

namespace Ticket2Up;

use Illuminate\Database\Eloquent\Model;

class TicketStatus extends Model
{
    public $timestamps = false;

    protected $fillable = ['ticket_id'];
}
