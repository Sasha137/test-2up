<?php

namespace Ticket2Up;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    protected $fillable = [
        'name', 'display_name', 'description'
    ];

    public function perms(){
        return $this->belongsToMany('Ticket2Up\Role');
    }
}