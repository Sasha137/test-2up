<?php

namespace Ticket2Up;

use Illuminate\Database\Eloquent\Model;

class TicketComment extends Model
{
    protected $fillable = ['comment', 'ticket_id', 'author_id'];
}
