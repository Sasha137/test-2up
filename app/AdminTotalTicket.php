<?php

namespace Ticket2Up;

use Illuminate\Database\Eloquent\Model;

class AdminTotalTicket extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'admin_id'
    ];
}
