<?php

namespace Ticket2Up;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    public function byRole(){

        return $this->belongsToMany('Ticket2Up\User');
        
    }
}
