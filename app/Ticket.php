<?php

namespace Ticket2Up;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['title', 'description', 'admin_id', 'user_id'];

    public function admin()
    {
        return $this->hasOne('Ticket2Up\User', 'id', 'admin_id');
    }

    public function user()
    {
        return $this->hasOne('Ticket2Up\User', 'id', 'user_id');
    }

    public function comments()
    {
        return $this->hasMany('Ticket2Up\TicketComment', 'ticket_id');
    }

    public function status()
    {
        return $this->hasOne('Ticket2Up\TicketStatus', 'ticket_id');
    }

    public function owner()
    {
        return $this->hasOne('Ticket2Up\TicketOwner', 'ticket_id');
    }
}
