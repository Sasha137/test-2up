<?php

use Illuminate\Database\Seeder;
use Ticket2Up\User;
use Ticket2Up\AdminTotalTicket;
use Ticket2Up\Role;
use Ticket2Up\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Создаем первого админа
        $primaryKey = User::create([
            'first_name' => 'Иванов',
            'last_name' => 'Александр',
            'email' => 'support@ticket2up.com',
            'password' => bcrypt('helloworld')
        ])->id;

        
        AdminTotalTicket::create([
            'admin_id' => $primaryKey
        ]);
        
        //Добавляем роль юзер
        $user = new Role();
        $user->name         = 'user';
        $user->display_name = 'Пользователь';
        $user->description  = 'Использовать web приложение';
        $user->save();
        
        //Добавляем роль админ
        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'Администратор';
        $admin->description  = 'Помощь и обслуживание пользователей';
        $admin->save();
        
        //Ставим первому юзеру роль админ
        $user = User::find($primaryKey);
        $user->roles()->attach($admin->id);
        
        //Добавляем разрешение для добавления тикетов
        $createTicket = new Permission();
        $createTicket->name         = 'add-ticket';
        $createTicket->display_name = 'Писать тикеты'; 
        $createTicket->description  = 'Писать администрации тикеты, коментарии к ним'; 
        $createTicket->save();
        
        //Добавляем разрешение для обработки тикета
        $replyTicket = new Permission();
        $replyTicket->name         = 'reply-ticket';
        $replyTicket->display_name = 'Отвечать на тикеты'; // optional
        $replyTicket->description  = 'Отвечать на тикеты пользователей'; // optional
        $replyTicket->save();
        
        //Ставим роли админа разрешения
        $admin->attachPermission($replyTicket);
    }
}
